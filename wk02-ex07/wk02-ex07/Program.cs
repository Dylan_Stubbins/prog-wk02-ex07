﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wk02_ex07
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter in a number");

            var i = int.Parse(Console.ReadLine());
            var counter = 20;

            do
            {
                var a = i + 1;

                Console.WriteLine($"This is line number {a}");

                i++;
            } while (i < counter);
        }
    }
}
